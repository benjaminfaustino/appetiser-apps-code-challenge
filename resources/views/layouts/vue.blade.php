<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Code Challenge</title>


    <link rel="stylesheet" href="{{ asset('/assets/dist/css/app.min.css') }}">

</head>
<body id="body" class="">
    
    <div id="app">
        <app></app>
    </div>

    <script type="text/javascript" src="{{ asset('minified/js/route.js') }}"></script>
</body>