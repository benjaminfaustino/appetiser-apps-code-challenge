const mix = require('laravel-mix');

/**
 * application code / app assets
 */

mix.js('resources/js/app.js', 'public/minified/js')
    .js('resources/js/routes/route.js', 'public/minified/js')
    .sass('resources/sass/app.scss', 'public/minified/css');

mix.styles([
    'public/assets/css/bootstrap.min.css',
], 'public/assets/dist/css/app.min.css');
