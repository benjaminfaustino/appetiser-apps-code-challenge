import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import App from '../components/layouts/App';
import Calendar from '../components/pages/Calendar';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/calendar',
            name: 'calendar',
            component: Calendar,
            props: { }
        } 
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});